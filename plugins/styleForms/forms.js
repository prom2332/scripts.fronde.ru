/* ==========================================================================
   Fronde styleForms - v1.1 - 2013-03-19
   Copyright (c) 2013 Fronde (http://fronde.ru)
   ========================================================================== */

(function($, undefined) {
    
    $.fn.styleForms = function(options) {
        var methods = {
            // �������������
            init : function() {
                return $(this).each(function() {
                    var type = $(this).attr('type') || this.nodeName.toLowerCase(),
                        data = $(this).data('styleForms');
                            
                    if (!data)
                    {
                        $(this).data('styleForms', {
                            target : $(this)
                        });
                        
                        switch (type)
                        {
                            case 'text':
                                methods.text.init($(this));
                                break;
                                
                            case 'password':
                                methods.text.init($(this));
                                break;
                                
                            case 'textarea':
                                methods.textarea.init($(this));
                                break;
                                
                            case 'radio':
                                methods.radio.init($(this));
                                break;
                                
                            case 'checkbox':
                                methods.checkbox.init($(this));
                                break;
                                
                            case 'select':
                                methods.select.init($(this));
                                break;
                                
                            case 'file':
                                methods.file.init($(this));
                                break;
                        }
                    }
                });
            },
            
            destroy : function() {
                return this.each(function() {
                    var data = $(this).data('styleForms');
                    $(this).removeData('styleForms');
                    
                    if ($(this).parents('.' + settings.className))
                    {
                        $(this).insertBefore($(this).parents('.' + settings.className));
                        $(this).next().remove();
                    }
                });
            },
            
            // ��������� ����
            text : {
                // �������������
                init : function(self) {
                    methods.text.style(self);
                    self.bind('keyup', methods.keyup);
                    self.bind('change', methods.keyup);
                    self.trigger('keyup');
                    
                    self.bind('focus', methods.focus);
                    self.bind('blur', methods.blur);
                    self.bind('mouseenter', methods.mouseenter);
                    self.bind('mouseleave', methods.mouseleave);
                },
                
                // ���������� �����
                style : function(self) {
                    self.wrap('<div class="' + settings.className + ' ' + settings.className + '_text" />');
                    self.parent().prepend('<div class="' + settings.classNamePlaceholder + '" />');
                    
                    methods.setPlaceholder(self);
                }
            },
            
            // Textarea
            textarea : {
                // �������������
                init : function(self) {
                    methods.textarea.style(self);
                    self.bind('keyup', methods.keyup);
                    self.bind('change', methods.keyup);
                    self.trigger('keyup');
                    
                    self.bind('focus', methods.focus);
                    self.bind('blur', methods.blur);
                    self.bind('mouseenter', methods.mouseenter);
                    self.bind('mouseleave', methods.mouseleave);
                },
                
                // ���������� �����
                style : function(self) {
                    if (!self.hasClass(settings.classNameNot))
                    {
                        self.wrap('<div class="' + settings.className + ' ' + settings.className + '_textarea" />');
                        self.parent()
                            .prepend('<div class="' + settings.className + '_textarea_center" />')
                            .prepend('<div class="' + settings.className + '_textarea_top" />')
                            .prepend('<div class="' + settings.className + '_textarea_bottom" />')
                            .prepend('<div class="' + settings.classNamePlaceholder + '" />');
                        
                        methods.setPlaceholder(self);
                    }
                }
            },
            
            // Radio
            radio : {
                init : function(self) {
                    methods.radio.style(self);
                    methods.radio.defineChecked(self);
                    self.bind('change', methods.radio.change);
                    self.prev().bind('click', methods.radio.click);
                    self.prev().bind('mouseenter', methods.mouseenter);
                    self.prev().bind('mouseleave', methods.mouseleave);
                },
                
                style : function(self) {
                    self.wrap('<div class="' + settings.className + ' ' + settings.className + '_radio" />');
                    self.parent().prepend('<div class="' + settings.className + '_radio_icon" />');
                },
                
                defineChecked : function(self) {
                    if (self.attr('checked'))
                    {
                        self.parents('form').find('[name="' + self.attr('name') + '"]').parent().removeClass(settings.classNameChecked);
                        self.parent().addClass(settings.classNameChecked);
                    }
                    else
                    {
                        self.parent().removeClass(settings.classNameChecked);
                    }
                },
                
                click : function() {
                    $(this).parent().find(':input').trigger('click');
                    return false;
                },
                
                change : function() {
                    $(this).attr('checked', 'checked');
                    methods.radio.defineChecked($(this));
                }
            },
            
            // Checkbox
            checkbox : {
                init : function(self) {
                    methods.checkbox.style(self);
                    methods.checkbox.defineChecked(self);
                    self.bind(($.browser.msie && ($.browser.version == '8.0' || $.browser.version == '7.0') ? 'propertychange': 'change'), methods.checkbox.change);
                    self.prev().bind('click', methods.checkbox.click);
                    self.prev().bind('mouseenter', methods.mouseenter);
                    self.prev().bind('mouseleave', methods.mouseleave);
                },
                
                style : function(self) {
                    self.wrap('<div class="' + settings.className + ' ' + settings.className + '_checkbox" />');
                    self.parent().prepend('<div class="' + settings.className + '_checkbox_icon" />');
                },
                
                defineChecked : function(self) {
                    if (self.attr('checked'))
                    {
                        self.parent().addClass(settings.classNameChecked);
                    }
                    else
                    {
                        self.parent().removeClass(settings.classNameChecked);
                    }
                },
                
                click : function() {
                    $(this).parent().find(':input').trigger('click');
                    return false;
                },
                
                change : function() {
                    methods.checkbox.defineChecked($(this));
                }
            },
            
            // Select
            select : {
                init : function(self) {
                    methods.select.style(self);
                    self.parent().bind('mouseenter', methods.mouseenter);
                    self.parent().bind('mouseleave', methods.mouseleave);
                    self.remove();
                },
                
                style : function(self) {
                    self.wrap('<div class="' + settings.className + ' ' + settings.className + '_select"><div class="select_title"><div class="select_title_center"></div></div></div>');
                    self.parents('.select_title').prepend('<div class="select_arrow"></div>');
                    
                    // ���������� ������ � ���������� ��������� �����
                    var title, value, option = '';
                    self.find('option').each(function() {
                        if ($(this).attr('selected'))
                        {
                            title = $(this).html();
                            value = $(this).attr('value');
                        }
                        
                        option += '<li><a rel="' + $(this).attr('value') + '" href="#">' + $(this).html() + '</a></li>';
                    });
                    
                    // ���������� ������ ����� ������� � ������� ������ ��������� �������
                    self.parents('.select_title').before('<input type="hidden" name="' + self.attr('name') + '" value="' + value + '">');
                    self.parent().prepend(title);
                    self.parents('.' + settings.className + '_select').append('<div class="select_open"><div class="select_open_block"><div class="select_margin"><ul>' + option + '</ul></div></div></div>');
                    
                    // ������ ������� �� ���� �� ������ �������
                    self.parents('.select_title').bind('click', methods.select.open);
                    self.parents('.' + settings.className + '_select').find('.select_open li').bind('click', methods.select.clickList);
                },
                
                open : function() {
                    if (!$(this).parents('.' + settings.className + '_select').hasClass(settings.classNameSelectOpen))
                    {
                        // ��� ����� �� �������� ������ ������� ������� ������
                        $('body').bind('click', methods.select.close);
                        
                        // ��������� ����� active
                        $('.' + settings.className + '_select').removeClass(settings.classNameSelectOpen);
                        $(this).parents('.' + settings.className + '_select').addClass(settings.classNameSelectOpen);
                        
                        // ���������� ��� �������� ������� � ��������� ��
                        $('.' + settings.className + '_select .select_open').each(function() {
                            if (!$(this).parents('.' + settings.className + '_select').hasClass(settings.classNameSelectOpen))
                            {
                                $(this).parents('.' + settings.className + '_select').find('.select_title').unbind('click', methods.select.close);
                                $(this).slideUp(settings.selectSpeed);
                            }
                        });
                        
                        // ��������� ������� ������
                        $(this).parents('.' + settings.className + '_select').find('.select_open').slideDown(settings.selectSpeed, function() {
                            var height1 = $(this).parents('.' + settings.className + '_select').find('ul').height(),
                                height2 = parseInt($(this).parents('.' + settings.className + '_select').find('.select_margin').css('max-height'));
                            
                            if (height1 >= height2)
                            {
                                $(this).parents('.' + settings.className + '_select').find('.select_margin').jScrollPane(settings.scroller);
                            }
                        });
                    }
                    else
                    {
                        $('body').trigger('click');
                    }
                    
                    return false;
                },
                
                close : function(e) {
                    var close = false;
                    
                    if (e.target.getAttribute('class'))
                    {
                        var targetClass = e.target.getAttribute('class').split(' ')[0];
                        
                        if (!$('.' + targetClass).parents('.select_open').attr('class'))
                        {
                            close = true;
                        }
                    }
                    else
                    {
                        close = true;
                    }
                    
                    if (close)
                    {
                        $('body').unbind('click');
                        $('.' + settings.className + '_select').removeClass(settings.classNameSelectOpen);
                        $('.' + settings.className + '_select').find('.select_open').slideUp(settings.selectSpeed);
                    }
                },
                
                clickList : function() {
                    $(this).parents('.' + settings.className + '_select').find('.select_title_center').html($(this).find('a').html());
                    $(this).parents('.' + settings.className + '_select').find(':input').attr('value', $(this).find('a').attr('rel'));
                    $(this).parents('.select_open').slideUp(settings.selectSpeed);
                    $('.' + settings.className + '_select').removeClass(settings.classNameSelectOpen);
                    $(this).parents('.' + settings.className + '_select').find('.select_title').unbind('click', methods.select.close);
                    return false;
                }
            },
            
            // File
            file : {
                init : function(self) {
                    methods.file.style(self);
                    self.bind('change', methods.file.change);
                    
                    self.parent().find('.' + settings.className + '_file_button').bind('click', methods.file.click);
                    self.parent().find('.' + settings.className + '_file_button').bind('mouseenter', methods.mouseenter);
                    self.parent().find('.' + settings.className + '_file_button').bind('mouseleave', methods.mouseleave);
                    self.parent().find('.' + settings.className + '_file_button').bind('mousedown', methods.file.mousedown);
                    self.parent().find('.' + settings.className + '_file_button').bind('mouseup', methods.file.mouseup);
                },
                
                style : function(self) {
                    if (self.attr('value'))
                    {
                        var name = self.attr('value');
                    }
                    else
                    {
                        var name = settings.nameFileText;
                    }
                    
                    self.wrap('<div class="' + settings.className + ' ' + settings.className + '_file" />');
                    self.parent().prepend('<div class="' + settings.className + '_file_text">' + name + '</div>');
                    self.parent().prepend('<a href="#" class="' + settings.className + '_file_button">' + settings.nameFileButton + '</a>');
                },
                
                click : function() {
                    $(this).parent().find(':input').trigger('click');
                    return false;
                },
                
                change : function() {
                    $(this).parent().find('.' + settings.className + '_file_text').html($(this).attr('value'));
                },
                
                mousedown : function() {
                    $(this).parents('.' + settings.className).addClass(settings.classNameFocus);
                },
                
                mouseup : function() {
                    $(this).parents('.' + settings.className).removeClass(settings.classNameFocus);
                }
            },
            
            // Placeholder
            setPlaceholder : function(self) {
                if (self.attr('data-placeholder'))
                {
                    self.parents('.' + settings.className).find('.placeholder').html(self.attr('data-placeholder'));
                }
            },
            
            // ������� �� keyup
            keyup : function() {
                var placeholder = $(this).parents('.' + settings.className).find('.' + settings.classNamePlaceholder);
                
                if ($(this).val())
                {
                    $(this).parent().addClass(settings.classNameNotEmpty);
                    placeholder.hide();
                }
                else
                {
                    $(this).parent().removeClass(settings.classNameNotEmpty);
                    placeholder.show();
                }
            },
            
            // ������� �� focus
            focus : function() {
                $(this).parents('.' + settings.className).addClass(settings.classNameFocus);
            },
            
            // ������� �� blur
            blur : function() {
                $(this).parents('.' + settings.className).removeClass(settings.classNameFocus);
            },
            
            // ������� �� mouseenter
            mouseenter : function() {
                $(this).parents('.' + settings.className).addClass(settings.classNameHover);
            },
            
            // ������� �� mouseleave
            mouseleave : function() {
                $(this).parents('.' + settings.className).removeClass(settings.classNameHover);
            }
        },
        
        // �������� ���������
        settings = $.extend(true, {}, $.fn.styleForms.defaults, options);
        
        // ������������� �������
        if (methods[options])
        {
            return methods[options].apply(this, Array.prototype.slice.call(arguments, 1));
        }
        else if (typeof options === 'object' || !options)
        {
            return methods.init.apply(this, arguments);
        }
        else
        {
            $.error('����� � ������ ' +  options + ' �� ����������');
        }
    };
    
    $.fn.styleForms.defaults = {
        className : 'forms',
        classNameNot : 'not_style',
        classNameFocus : 'focus',
        classNameNotEmpty : 'not_empty',
        classNameHover : 'hover',
        classNamePlaceholder : 'placeholder',
        classNameChecked : 'checked',
        classNameSelectOpen : 'open',
        nameFileText : '�������� ���� ��� ��������',
        nameFileButton : '�����',
        selectSpeed : 250,
        scroller : {
            showArrows: true,
            verticalGutter: 3
        }
    }
    
})(jQuery);