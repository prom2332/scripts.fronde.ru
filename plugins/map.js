/*  ==========================================================================
    Fronde yandexMaps - v0.1 - 2013-02-14
    Copyright (c) 2013 Fronde (http://fronde.ru)

    map (object)
        type (string):
            "yandex#map" - тип карты "схема";
            "yandex#satellite" - тип карты "спутник";
            "yandex#hybrid" - тип карты "гибрид";
            "yandex#publicMap" - тип карты "народная карта";
            "yandex#publicMapHybrid" - тип карты "народный гибрид".

        behaviors (string, array):
            "default" - короткий синоним для включения/отключения поведений карты по умолчанию;
            "drag" - перемещание карты при нажатой левой кнопке мыши либо одиночным касанием behavior.Drag;
            "scrollZoom" - изменение масштаба колесом мыши behavior.ScrollZoom;
            "dblClickZoom" - масштабирование карты двойным щелчком кнопки мыши behavior.DblClickZoom;
            "multiTouch" - масштабирование карты двойным касанием (например, пальцами на сенсорном экране) behavior.MultiTouch;
            "rightMouseButtonMagnifier" - увеличение области, выделенной правой кнопкой мыши (только для настольных браузеров), behavior.RightMouseButtonMagnifier;
            "leftMouseButtonMagnifier" - увеличение области, выделенной левой кнопкой мыши либо одиночным касанием, behavior.LeftMouseButtonMagnifier;
            "ruler" - измерение расстояния behavior.Ruler;
            "routeEditor" - редактор маршрутов behavior.RouteEditor;

        center (array):

        zoom (number):
            1 - 23

        minZoom (number):
            Минимальный коэффициент масштабирования карты.
            Значение по умолчанию: 0

        maxZoom (number):
            Максимальный коэффициент масштабирования карты.
            Значение по умолчанию: 23
            
    controls (array):
        zoomControl (boolean):
            Добавление на карту панели изменения масштаба
            Значение по умолчанию: true
            
        smallZoomControl (boolean):
            Добавление на карту кнопок изменения масштаба
            Значение по умолчанию: false
            
        typeSelector (boolean):
            Добавление на карту переключателя типа карты (в выпадающем списке будут отображены все допустимые типы)
            Значение по умолчанию: true
            
        mapTools (boolean):
            Добавление на карту панели инструментов со стандартным набором кнопок (рука, лупа, линейка)
            Значение по умолчанию: true
            
        scaleLine (boolean):
            Добавление на карту масштабной линейки
            Значение по умолчанию: true
            
        miniMap (boolean):
            Добавление обзорной карты
            Значение по умолчанию: true
            
        searchControl (boolean):
            Добавление на карту поисковой строки
            Значение по умолчанию: false
            
        trafficControl (boolean):
            Добавление на карту элемента управления «Пробки»
            Значение по умолчанию: true
        
    data (array objects):
        type:
            routes - построение маршрута на карте
            placemark - установка точки на карте
        
        addresses:
            type == routes (array) - Адреса в массиве
            type == placemark (string) - Адрес или координаты строкой
            
        complete:
            Метод при выполнении действия. Возвращает obj:
                type == routes:
                    obj.getLength - расстояние от первой до последней точки
                    obj.getJamsTime - Время в пути с учетом пробок
                    obj.getTime - Время в пути без учета пробок
            
        
   
    ========================================================================== */


(function($, undefined) {
    
    $.fn.yandexMaps = function(options) {
        // Создаем временные переменные, чтобы передать this и arguments в yandex ready
        var self = this,
            argument = arguments;
            
        ymaps.ready(function() {
            var methods = {
                // init
                __init : function() {
                    return $(this).each(function() {
                        // create map and controls
                        methods.__map.create.apply($(this));
                        methods.__map.control.apply($(this));
                        
                        // events click for the map
                        map.events.add('click', function() {
                            settings.map.click(map);
                        });
                        
                        // handle data and call methods
                        if (settings.data)
                        {
                            var self = $(this);
                            
                            $.each(settings.data, function(key, value) {
                                if (value.type == 'routes')
                                {
                                    return methods.__route.add.apply(self, [value]);
                                }
                                else if (value.type == 'placemark')
                                {
                                    return methods.__placemark.add.apply(self, [value]);
                                }
                            });
                        }
                        
                        if (settings.getCoords)
                        {
                            map.events.add('click', methods.__getCoords);
                        }
                        
                        var $this = $(this),
                            data = $this.data('maps'),
                            maps = $('<div />', {
                                text : $this.attr('title')
                            });
                            
                        if (!data)
                        {
                            $(this).data('maps', {
                                target : $this,
                                map : map
                            });
                        }
                    });
                },
                
                // map methods
                __map : {
                    // create map
                    create : function() {
                        // get map id
                        if (!$(this).attr('id'))
                        {
                            $(this).attr('id', 'map_' + Math.random());
                        }
                        
                        // create map
                        map = new ymaps.Map($(this).attr('id'), {
                            center: settings.map.center,
                            zoom: settings.map.zoom,
                            behaviors: settings.map.behaviors,
                            type: settings.map.type
                        }, {
                            minZoom: settings.map.minZoom,
                            maxZoom: settings.map.maxZoom
                        });
                    },
                    
                    // create controls
                    control : function() {
                        $.each(settings.controls, function(key, value) {
                            if (value == true)
                            {
                                map.controls.add(key);
                            }
                        });
                    }
                },
                
                // route methods
                __route : {
                    // add route
                    add : function(data) {
                        // check data
                        if (typeof data.addresses === 'object')
                        {
                            var self = $(this);
                            
                            ymaps.route(data.addresses, {
                                mapStateAutoApply:true
                            }).then(function (route) {
                                // draw route
                                map.geoObjects.add(route);
                                
                                // complete draw
                                if (data.complete)
                                {
                                    return data.complete.apply(self, [route]);
                                }
                            });
                        }
                    },
                    
                    moveList : function() {
                        
                    }
                },
                
                // placemark methods
                __placemark : {
                    // add placemark
                    add : function(data) {
                        if (data.method == 'geocode')
                        {
                            methods.__placemark.create(data);
                            
                            // complete draw
                            if (data.complete)
                            {
                                return data.complete.apply(self);
                            }
                        }
                        else if (data.method == 'coordinates')
                        {
                            
                        }
                    },
                    
                    // create placemark
                    create : function(data) {
                        ymaps.geocode(data.addresses[0], { results: 1 }).then(function (res) {
                            // Выбираем первый результат геокодирования
                            var firstGeoObject = res.geoObjects.get(0),
                                coords = firstGeoObject.geometry.getCoordinates(),
                                myPlacemark;
                                
                            myPlacemark = new ymaps.Placemark(coords, {
                                iconContent: data.properties.name,
                                balloonContentHeader: data.properties.title,
                                balloonContentBody: data.properties.content,
                            }, {
                                preset: 'twirl#blueStretchyIcon' // иконка растягивается под контент
                            });
                            
                            cluster.add(myPlacemark);
                            map.geoObjects.add(cluster);
                            
                            if (data.setCenter == 'true')
                            {
                                map.setCenter(coords);
                            }
                        }, function (err) {
                            alert('Произошла ошибка при загрузке карты');
                        });
                    }
                },
                
                // get coords for click
                __getCoords : function(e) {
                    map.balloon.open(e.get("coordPosition"), {
                        contentBody: "position: " + e.get("coordPosition")
                    });
                },
                
                
                /*
                 * public methods
                 */
                 
                // destroy map
                destroy : function() {
                    return this.each(function(){
                        var data = $(this).data('maps');
                            
                        data.map.destroy();
                        $(this).removeData('maps');
                    });
                }
            }
            
            // merge settings and create var for map
            var settings = $.extend(true, {}, $.fn.yandexMaps.defaults, options),
                map,
                cluster = new ymaps.Clusterer();
            
            // init plugin
            if (methods[options])
            {
                // call methods
                return methods[options].apply(self, Array.prototype.slice.call(argument, 1));
            }
            else if (typeof options === 'object' || !options)
            {
                // standart call and transfer options
                return methods.__init.apply(self, argument);
            }
            else
            {
                // if not method
                $.error('Метод с именем ' +  options + ' не существует');
            }
        });
    };
    
    // settings
    $.fn.yandexMaps.defaults = {
        // settings map
        map : {
            type         : 'yandex#satellite',
            behaviors    : ['default', 'scrollZoom'],
            center       : [55.755695522962, 37.623130732297945],
            zoom         : 13,
            minZoom      : 0,
            maxZoom      : 23,
            click        : $.noop
        },
        
        // settings controls
        controls: {
            zoomControl         : true,
            smallZoomControl    : false,
            typeSelector        : true,
            mapTools            : true,
            scaleLine           : true,
            miniMap             : true,
            searchControl       : false,
            trafficControl      : true,
        },
        
        // settings other
        type : 'placemark',
        getCoords : false
        
        // methods
        
    }
    
})(jQuery);