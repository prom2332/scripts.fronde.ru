$(document).ready(function() {
    $('a[href=#]').on('click dblclick', false);
    
    // Plugins
    (function() {
        // Fancybox
        $('.zoom').fancybox();
        
        // ���������� ����
        $('.style_forms').styleForms();
        
        // Cookie
        // $.cookie('test', '1', {expires: 365, path: '/'});
        
        // Cloud-zoom
        $('.cloud-zoom').CloudZoom();
        
        // Slider
        $('#slides').slides({
            preload: true,
            bigTarget: true,
            hoverPause: true,
            play: 2500,
            pause: 2500,
            hoverPause: true
        });
        
        // pixlayout
        $.pixlayout({
            src: 'images/screenshots/main.jpg',
            opacity: 0.3,
            top: 50,
            center: true,
            clip: true
        });
    })();
    
    // popup
    (function() {
        // Open
        $('.popup_open').click(function() {
            $('.popup_bg, .popup').css({'opacity': '1', 'visibility': 'visible'});
            $('.popup').css('top', ($(window).scrollTop() + 100) + 'px');
        });
        
        // Open gradient
        $('.popup_open_gradient').click(function() {
            $('.popup_bg, .popup_gradient').css({'opacity': '1', 'visibility': 'visible'});
            $('.popup_gradient').css('top', ($(window).scrollTop() + 100) + 'px');
        });
        
        // Close
        $('.popup .close, .popup_gradient .close').click(function() {
            $('.popup_bg, .popup, .popup_gradient').css({'opacity': '0', 'visibility': 'hidden'});
        });
    })();
    
    
    
});